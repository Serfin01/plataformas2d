﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PelotaLauncher : MonoBehaviour
{
    [SerializeField] GameObject pelota;
    [SerializeField] float tiempo = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LanzarPelotas());
    }

    IEnumerator LanzarPelotas()
    {
        GameObject go;
        while (true)
        {
            go = Instantiate(pelota);
            go.transform.position = transform.position;
            yield return new WaitForSeconds(tiempo);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 1);
    }
}
